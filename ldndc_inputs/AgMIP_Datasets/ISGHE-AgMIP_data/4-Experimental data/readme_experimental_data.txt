﻿"These files contain all data described in the data article: P Martre, MP Reynolds,  S Asseng, F Ewert, PD Alderman, D Cammarano, AC Ruane, PK Aggarwal, J Anothai, B Basso, C Biernath, AJ Challinor, G De Sanctis, J Doltra, E Fereres, M Garcia-Vila, S Gayler, G Hoogenboom, LA Hunt, RC Izaurralde, M Jabloun, CD Jones, KC Kersebaum, A-K Koehler, C Müller, S Naresh Kumar, DB Lobell, A Maiorano, C Nendel, G O’Leary, JE Olesen, T Palosuo, E Priesack, E Eyshi Rezaei, D Ripoche, RP Rötter, MA Semenov, Iurii Shcherbak, Claudio Stöckle, Pierre Stratonovitch, Thilo Streck, Iwan Supit, F Tao, P Thorburn, K Waha, E Wang, JW White, J Wolf, Z Zhao, Y Zhu"
The International Heat Stress Genotype Experiment for modeling wheat response to heat: field experiments and AgMIP-Wheat multi-model simulations
Open Data Journal for Agricultural Research : ODjAR  and is included in this root directory. 

This folder: Experimental data contains mean and standard deviation (sd) of crop measuremnts 

Data is provided  as tab delimited text files formatted for input or as output of the R routines developed in the AgMIP 
"effort on Crop growth model intercomparison effort (http://www.agmip.org/). See especially the ""tools"" section for further reference"

ISGHE_AgMIP_ measurement_key.txt
ISGHE_AgMIP_measurments_ave_sd.txt

readme_experimental_data.txt
