﻿Name	Unit	Description	Calculation	ICASA standard name	ICASA standard unit	ICASA standard conversion factor	Comment
Model	-	2-letter model code	-	-	-	-	-
Version	-	Original or improved	-	-	-	-	Original and improved refere to the model versions in Maiorano et al., (2016), http://dx.doi.org/10.1016/j.fcr.2016.05.001. Simulations with original models are the same as in Asseng et al. (2015) http://dx.doi.org/10.1038/nclimate2470 (Blind test with calibrated highest yield, step D).
Country	-	-	-	FL_LOC_1	-	-	-
Site	-	-	-	SITE_NAME	-	-	-
Variety	-	-	-	CUL_NAME	-	-	-
Year	yyyy	Harvest year	-	HDATE	yyyy-mm-dd	-	Combine Year with harvest day of year to calculate HDATE
Sowing	-	N = normal, L late	-	PL	-	-	-
Date	yyyy-mm-dd	Date of simulation	-	DATE	yyyy-mm-dd	-	-
DAS	day	Days after sowing	DAP	-	-	Date difference function(DATE, PDATE)	-
Ant.date	yyyy-mm-dd	Time to anthesis (DC65)	-	ADAT	yyyy-mm-dd	-	-
Mat.date	yyyy-mm-dd	Time to maturity (DC90)	-	MDAT	yyyy-mm-dd	-	-
GrainDM	t DM/ha	In season grain dry mass	-	GWAD	kg/ha	1000	-
Yield	t DM/ha	Grain dry mass at maturity	-	GWAM	kg/ha	1000	-
Biom	t DM/ha	Total above ground biomass	-	CWAD	kg/ha	1000	-
Biom.an	t DM/ha	Total above ground biomass at anthesis	-	CWAA	kg/ha	1000	-
Biom.ma	t DM/ha	Total above ground biomass at maturity	-	CWAM	kg/ha	1000	-
HI	%	Dry mass harvest index at maturity	100 * Yield / Biom.ma	HIAM	fraction	0.01	-
GNumber	grain/m²	Grain number	-	H#AM	grain/m²	1	-
GDM	mg DM/grain	In-season single grain dry mass	10^5 * Yield / GNumber	GWGD	mg	1	-
GDM.ma	mg DM/grain	Single grain dry mass at maturity	10^5 * GrainDM / GNumber	GWGM	mg	1	-
LAI	m²/m²	In-season leaf area index	-	LAID	m²/m²	1	-
LAI.an	m²/m²	Leaf area index at anthesis	-	LAIX	m²/m²	1	-
ET	mm	In-season cumulative evapotranspiration	-	ETCP	mm	1	-
ET.ma	mm	Cumulative evapotranspiration at maturity	-	ETCM	mm	1	-
Transp	mm	In-season cumulative transpiration	-	EPCM	mm	1	-
Transp.ma	mm	Cumulative transpiration at maturity	-	EPCP	mm	1	-
