﻿! estimated from previous study				
! Definitions	Soil layer base depth	Initial water content	Initial ammonium conc	Initial nitrate conc
! Units	cm	mm3/mm3	ppm	ppm
%	icbl	ich2o	icnh4	icno3
	5	0.150	0.8	24.9
	15	0.150	0.8	24.9
	30	0.199	0.6	16.0
	50	0.318	0.4	9.8
	60	0.318	0.4	9.8
	70	0.318	0.2	2.1
	90	0.299	0.2	2.1
	110	0.275	0.2	2.1
	120	0.275	0.2	2.1
	130	0.275	0.2	0.2
	150	0.254	0.2	0.2
	170	0.254	0.2	0.2
	190	0.244	0.2	0.2
	210	0.244	0.2	0.2
