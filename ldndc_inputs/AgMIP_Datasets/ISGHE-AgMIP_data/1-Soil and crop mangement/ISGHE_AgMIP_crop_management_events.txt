﻿! Treatment	Type of event		 Date of Event code 	Sowing date (estimated from emergence date; 7 days before emergence)         yyyy-mm-dd	Crop ID code 	 Wheat code	Cultivar ID code	text	Additional data about cultivar	text_More details in the “cv_NOTES” sheet	Plant population code 	Plant Population (#/m2)	Row spacing (PLRS) 	Row spacing (cm)	Planting depth code 	Planting depth (cm)
1	Event	Planting	pdate	1990-11-26	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	240	PLRS	20	PLDP	5
2	Event	Planting	pdate	1990-11-26	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	240	PLRS	20	PLDP	5
3	Event	Planting	pdate	1991-01-30	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	107	PLRS	20	PLDP	5
4	Event	Planting	pdate	1991-01-30	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	130	PLRS	20	PLDP	5
7	Event	Planting	pdate	1993-03-08	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	107	PLRS	20	PLDP	5
8	Event	Planting	pdate	1993-03-08	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	130	PLRS	20	PLDP	5
9	Event	Planting	pdate	1990-12-03	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	301	PLRS	20	PLDP	5
10	Event	Planting	pdate	1990-12-03	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	338	PLRS	20	PLDP	5
11	Event	Planting	pdate	1991-02-24	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	248	PLRS	20	PLDP	5
12	Event	Planting	pdate	1991-02-24	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	282	PLRS	20	PLDP	5
13	Event	Planting	pdate	1991-12-03	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	253	PLRS	20	PLDP	5
14	Event	Planting	pdate	1991-12-03	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	262	PLRS	20	PLDP	5
15	Event	Planting	pdate	1992-03-02	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	170	PLRS	20	PLDP	5
16	Event	Planting	pdate	1992-03-02	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	205	PLRS	20	PLDP	5
17	Event	Planting	pdate	1990-11-27	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	152	PLRS	20	PLDP	5
18	Event	Planting	pdate	1990-11-27	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	226	PLRS	20	PLDP	5
19	Event	Planting	pdate	1991-11-15	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	306	PLRS	20	PLDP	5
20	Event	Planting	pdate	1991-11-15	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	358	PLRS	20	PLDP	5
21	Event	Planting	pdate	1990-12-19	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	181	PLRS	20	PLDP	5
22	Event	Planting	pdate	1990-12-19	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	251	PLRS	20	PLDP	5
23	Event	Planting	pdate	1991-12-11	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	228	PLRS	20	PLDP	5
24	Event	Planting	pdate	1991-12-11	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	314	PLRS	20	PLDP	5
25	Event	Planting	pdate	1990-12-08	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	178	PLRS	20	PLDP	5
26	Event	Planting	pdate	1990-12-08	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	265	PLRS	20	PLDP	5
27	Event	Planting	pdate	1991-12-10	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	372	PLRS	20	PLDP	5
28	Event	Planting	pdate	1991-12-10	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	349	PLRS	20	PLDP	5
33	Event	Planting	pdate	1991-12-27	CRID	WHB	CUL_ID	Bacanora 88	cv_NOTES	"low photoperiod sensitivity, moderate  vernalization requirement"	PLPOP	123	PLRS	20	PLDP	5
34	Event	Planting	pdate	1991-12-27	CRID	WHB	CUL_ID	Nesser	cv_NOTES	"low photoperiod sensitivity, low vernalization requirement"	PLPOP	147	PLRS	20	PLDP	5
