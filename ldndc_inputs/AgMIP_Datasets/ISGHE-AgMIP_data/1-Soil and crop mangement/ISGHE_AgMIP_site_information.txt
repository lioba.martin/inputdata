﻿! Definitions	Experiment name	treatment name			Location	Duration of experiment	Latitude	Longitude	Field Elevation	Weather station id	Soil id	
! Units						year	decimal degrees	decimal degrees	m	Data have .wth extension	Soil in the next sheet	
# Treatment	exname	TRT_NAME	institution	site_name	local_name	exp_dur	fl_lat	fl_long	flele	wst_id	soil_id	dome_name
1	International Heat Stress Genotype	OBR-N-91-Bac	CIMMYT	"Centro Experimental Norman E. Borlaug, Ciudad Obregon, Mexico"	Obregon	2	27.397	-109.924	38	MXOB0001	AZMC920001	
2	International Heat Stress Genotype	OBR-N-91-Nes	CIMMYT	"Centro Experimental Norman E. Borlaug, Ciudad Obregon, Mexico"	Obregon	2	27.397	-109.924	38	MXOB0001	AZMC920001	
3	International Heat Stress Genotype	OBR-L-91-Bac	CIMMYT	"Centro Experimental Norman E. Borlaug, Ciudad Obregon, Mexico"	Obregon	2	27.397	-109.924	38	MXOB0001	AZMC920001	
4	International Heat Stress Genotype	OBR-L-91-Nes	CIMMYT	"Centro Experimental Norman E. Borlaug, Ciudad Obregon, Mexico"	Obregon	2	27.397	-109.924	38	MXOB0001	AZMC920001	
7	International Heat Stress Genotype	OBR-L-93-Bac	CIMMYT	"Centro Experimental Norman E. Borlaug, Ciudad Obregon, Mexico"	Obregon	2	27.397	-109.924	38	MXOB0001	AZMC920001	
8	International Heat Stress Genotype	OBR-L-93-Nes	CIMMYT	"Centro Experimental Norman E. Borlaug, Ciudad Obregon, Mexico"	Obregon	2	27.397	-109.924	38	MXOB0001	AZMC920001	
9	International Heat Stress Genotype	TLT-N-91-Bac	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
10	International Heat Stress Genotype	TLT-N-91-Nes	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
11	International Heat Stress Genotype	TLT-L-91-Bac	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
12	International Heat Stress Genotype	TLT-L-91-Nes	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
13	International Heat Stress Genotype	TLT-N-92-Bac	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
14	International Heat Stress Genotype	TLT-N-92-Nes	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
15	International Heat Stress Genotype	TLT-L-92-Bac	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
16	International Heat Stress Genotype	TLT-L-92-Nes	CIMMYT	"CIMMYT Experiment Station, Tlaltizapan, Mexico"	Tlaltizapan	2	18.69	-99.126	940	MXTL0001	AZMC920001	
17	International Heat Stress Genotype	SUD-N-91-Bac	"CIMMYT-ARC,Sudan"	"ARC, Wad Medani, Sudan"	Wad Medani	2	14.404	33.49	411	SUWM0001	AZMC920001	
18	International Heat Stress Genotype	SUD-N-91-Nes	"CIMMYT-ARC,Sudan"	"ARC, Wad Medani, Sudan"	Wad Medani	2	14.404	33.49	411	SUWM0001	AZMC920001	
19	International Heat Stress Genotype	SUD-N-92-Bac	"CIMMYT-ARC,Sudan"	"ARC, Wad Medani, Sudan"	Wad Medani	2	14.404	33.49	411	SUWM0001	AZMC920001	
20	International Heat Stress Genotype	SUD-N-92-Nes	"CIMMYT-ARC,Sudan"	"ARC, Wad Medani, Sudan"	Wad Medani	2	14.404	33.49	411	SUWM0001	AZMC920001	
21	International Heat Stress Genotype	IND-N-91-Bac	CIMMYT-IARI	"IARI, Dharwar, India"	Dharwar	2	15.485	74.977	638	INDH0001	AZMC920001	
22	International Heat Stress Genotype	IND-N-91-Nes	CIMMYT-IARI	"IARI, Dharwar, India"	Dharwar	2	15.485	74.977	638	INDH0001	AZMC920001	
23	International Heat Stress Genotype	IND-N-92-Bac	CIMMYT-IARI	"IARI, Dharwar, India"	Dharwar	2	15.485	74.977	638	INDH0001	AZMC920001	
24	International Heat Stress Genotype	IND-N-92-Nes	CIMMYT-IARI	"IARI, Dharwar, India"	Dharwar	2	15.485	74.977	638	INDH0001	AZMC920001	
25	International Heat Stress Genotype	BNG-N-91-Bac	"CIMMYT-WRC,Bangladesh"	"WRC, Dinajpur, Bangladesh"	Dinajpur	2	25.65	88.68	29	BDDI0001	AZMC920001	
26	International Heat Stress Genotype	BNG-N-91-Nes	"CIMMYT-WRC,Bangladesh"	"WRC, Dinajpur, Bangladesh"	Dinajpur	2	25.65	88.68	29	BDDI0001	AZMC920001	
27	International Heat Stress Genotype	BNG-N-92-Bac	"CIMMYT-WRC,Bangladesh"	"WRC, Dinajpur, Bangladesh"	Dinajpur	2	25.65	88.68	29	BDDI0001	AZMC920001	
28	International Heat Stress Genotype	BNG-N-92-Nes	"CIMMYT-WRC,Bangladesh"	"WRC, Dinajpur, Bangladesh"	Dinajpur	2	25.65	88.68	29	BDDI0001	AZMC920001	
33	International Heat Stress Genotype	EGP-N-92-Bac	"CIMMYT-ARC,Egypt"	"ARC, Aswan, Egypt"	Aswan	2	24.1	32.9	200	EGSA0001	AZMC920001	
34	International Heat Stress Genotype	EGP-N-92-Nes	"CIMMYT-ARC,Egypt"	"ARC, Aswan, Egypt"	Aswan	2	24.1	32.9	200	EGSA0001	AZMC920001	
