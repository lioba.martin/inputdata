﻿"These files contain all data described in the data article: P Martre, MP Reynolds,  S Asseng, F Ewert, PD Alderman, D Cammarano, AC Ruane, PK Aggarwal, J Anothai, B Basso, C Biernath, AJ Challinor, G De Sanctis, J Doltra, E Fereres, M Garcia-Vila, S Gayler, G Hoogenboom, LA Hunt, RC Izaurralde, M Jabloun, CD Jones, KC Kersebaum, A-K Koehler, C Müller, S Naresh Kumar, DB Lobell, A Maiorano, C Nendel, G O’Leary, JE Olesen, T Palosuo, E Priesack, E Eyshi Rezaei, D Ripoche, RP Rötter, MA Semenov, Iurii Shcherbak, Claudio Stöckle, Pierre Stratonovitch, Thilo Streck, Iwan Supit, F Tao, P Thorburn, K Waha, E Wang, JW White, J Wolf, Z Zhao, Y Zhu"
The International Heat Stress Genotype Experiment for modeling wheat response to heat: field experiments and AgMIP-Wheat multi-model simulations
Open Data Journal for Agricultural Research : ODjAR  and is included in this root directory. 

This folder: Model husbandry contains information about soil and crop husbandry parameters used as input in the model

The data is provided in a Microsoft Excel workbook saved in Excel 2003 xml format and in tab delimited text files

ISGHE_AgMIP_husbandry.xlm
ISGHE_AgMIP_crop_management_events.txt
ISGHE_AgMIP_site_information.txt
ISGHE_AgMIP_soil_description.txt
ISGHE_AgMIP_soil_initial_conditions.txt
ISGHE_AgMIP_treatment_key.txt

readme_model_name.txt
