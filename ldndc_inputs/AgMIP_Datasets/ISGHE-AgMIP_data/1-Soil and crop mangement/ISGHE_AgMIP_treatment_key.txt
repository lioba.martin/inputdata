﻿# Treatment	Location	Sowing	Year	Cultivar			
01	"Ciudad Obregon, Mexico"	N	1991	Bacanora 88			
02	"Ciudad Obregon, Mexico"	N	1991	Nesser			
03	"Ciudad Obregon, Mexico"	L	1991	Bacanora 88			
04	"Ciudad Obregon, Mexico"	L	1991	Nesser			
07	"Ciudad Obregon, Mexico"	L	1993	Bacanora 88			
08	"Ciudad Obregon, Mexico"	L	1993	Nesser			
09	"Tlatizapan, Mexico"	N	1991	Bacanora 88			
10	"Tlatizapan, Mexico"	N	1991	Nesser			
11	"Tlatizapan, Mexico"	L	1991	Bacanora 88			
12	"Tlatizapan, Mexico"	L	1991	Nesser			
13	"Tlatizapan, Mexico"	N	1992	Bacanora 88			
14	"Tlatizapan, Mexico"	N	1992	Nesser			
15	"Tlatizapan, Mexico"	L	1992	Bacanora 88			
16	"Tlatizapan, Mexico"	L	1992	Nesser			
17	"Wad Medani, Sudan"	N	1991	Bacanora 88			
18	"Wad Medani, Sudan"	N	1991	Nesser			
19	"Wad Medani, Sudan"	N	1992	Bacanora 88			
20	"Wad Medani, Sudan"	N	1992	Nesser			
21	"Dharwar, India"	N	1991	Bacanora 88			
22	"Dharwar, India"	N	1991	Nesser			
23	"Dharwar, India"	N	1992	Bacanora 88			
24	"Dharwar, India"	N	1992	Nesser			
25	"Dinajpur, Bangladesh"	N	1991	Bacanora 88			
26	"Dinajpur, Bangladesh"	N	1991	Nesser			
27	"Dinajpur, Bangladesh"	N	1992	Bacanora 88			
28	"Dinajpur, Bangladesh"	N	1992	Nesser			
33	"Aswan, Egypt"	N	1992	Bacanora 88			
34	"Aswan, Egypt"	N	1992	Nesser			
