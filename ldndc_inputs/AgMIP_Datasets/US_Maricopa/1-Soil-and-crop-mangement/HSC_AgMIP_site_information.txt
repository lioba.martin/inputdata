! Definitions	Experiment name	treatment name			Location	Duration of experiment	Latitude	Longitude	Field Elevation	Weather station id	Soil id	
! Units						year	decimal degrees	decimal degrees	m	Data have .wth extension	Soil in the next sheet	
# Treatment	exname	TRT_NAME	institution	site_name	local_name	exp_dur	fl_lat	fl_long	flele	wst_id	soil_id	dome_name
1	Hot Serial Cereal	13MAR07 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
4	Hot Serial Cereal	13MAR07 H	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ110306	AZMC920001	
10	Hot Serial Cereal	19APR07 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
13	Hot Serial Cereal	12JUN07 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
16	Hot Serial Cereal	25JUL07 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
31	Hot Serial Cereal	02JAN08 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
34	Hot Serial Cereal	02JAN08 H	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ310306	AZMC920001	
40	Hot Serial Cereal	13FEB08 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
43	Hot Serial Cereal	10MAR08 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
46	Hot Serial Cereal	10MAR08 H	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ440306	AZMC920001	
52	Hot Serial Cereal	28APR08 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
55	Hot Serial Cereal	25AUG08 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
70	Hot Serial Cereal	01DEC08 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
73	Hot Serial Cereal	01DEC08 H	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ610306	AZMC920001	
79	Hot Serial Cereal	12JAN09 C	"USDA ARS, Arid Land Agricultural Center"	"Maricopa Agric. Center, Univ. Arizona"	"Maricopa, AZ"	2	33.069	-114.53	361	AZ000306	AZMC920001	
