"These files contain all data described in the data article: P. Martre, B. Kimball, J.W. White, S. Asseng, F. Ewert, M.J. Ottman, G.W. Wall, D. Cammarano, A.C. Ruane, P.K. Aggarwal, J. Anothai, B. Basso, C. Biernath, A.J. Challinor, G. De Sanctis, J. Doltr"
The Hot Serial Cereal Experiment for modeling wheat response to temperature: field experiments and AgMIP-Wheat multi-model simulations
Open Data Journal for Agricultural Research : ODjAR  and is included in this root directory. 

This folder: Model husbandry contains information about soil and crop husbandry parameters used as input in the model

The data is provided in a Microsoft Excel workbook saved in Excel 2003 xml format and in tab delimited text files

HSC_AgMIP_site_soil_crop_management.xml
HSC_AgMIP_crop_management_events.txt
HSC_AgMIP_fertilizer.txt
HSC_AgMIP_irrigation.txt
HSC_AgMIP_site_information.txt
HSC_AgMIP_soil_description.txt
HSC_AgMIP_soil_initial_conditions.txt
HSC_AgMIP_soil_residue.txt
HSC_AgMIP_treatment_key.txt
HSC_AgMIP_variety_information.txt

readme_model_name.txt
