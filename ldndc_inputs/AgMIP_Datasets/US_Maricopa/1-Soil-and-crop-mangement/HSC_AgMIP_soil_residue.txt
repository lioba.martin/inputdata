! Definitions	Initial conditions date	Previous crop code	Initial surface residue	Initial root residue	Initial residue N conc		Initial root residue
! Treatments	yyyy-mm-dd	Wheat (WH)	g[dry matter]/m2	g[dry matter]/m2	%		g[dry matter]/m2
#	icdat	icpcr	icrag	icrt	icrn		! icrt
1	2007-03-13	WH	0			!Average	133
4	2007-03-13	WH	0			!StDev	34
10	2007-04-19	WH	0			! Min	89
13	2007-06-12	WH	0			!Max	185
16	2007-07-25	WH	0				
31	2008-01-02	WH	588				
34	2008-01-02	WH	588				
40	2008-02-13	WH	0				
43	2008-03-10	WH	0				
46	2008-03-10	WH	0				
52	2008-04-28	WH	983				
55	2008-08-25	WH	581				
70	2008-12-01	WH	838				
73	2008-12-01	WH	838				
79	2009-01-12	WH	0				
