"These files contain all data described in the data article: P. Martre, B. Kimball, J.W. White, S. Asseng, F. Ewert, M.J. Ottman, G.W. Wall, D. Cammarano, A.C. Ruane, P.K. Aggarwal, J. Anothai, B. Basso, C. Biernath, A.J. Challinor, G. De Sanctis, J. Doltr"
The Hot Serial Cereal Experiment for modeling wheat response to temperature: field experiments and AgMIP-Wheat multi-model simulations
Open Data Journal for Agricultural Research : ODjAR  and is included in this root directory. 

This folder: Model output contains summary and daily model output

Data is provided  as tab delimited text files formatted for input or as output of the R routines developed in the AgMIP 
"effort on Crop growth model intercomparison effort (http://www.agmip.org/). See especially the ""tools"" section for further reference"

Files
HSC_AgMIP_ simulation_key.txt
HSC_AgMIP_daily_simulations.txt
HSC_AgMIP_summary_simulations.txt

readme_experimental_data.txt
