Name	Unit	Description	Calculation	ICASA standard name	ICASA standard unit	ICASA standard conversion factor	Comment
Model	-	2-letter model code	-	-	-	-	-
Version	-	Original or improved	-	-	-	-	Original and improved refere to the model versions in Maiorano et al., (2016), http://dx.doi.org/10.1016/j.fcr.2016.05.001. Simulations with original models are the same as in Asseng et al. (2015) http://dx.doi.org/10.1038/nclimate2470 (Blind test with calibrated highest yield, step D).
PNO	-	Planting number	-	-	-	-	-
Trt	-	"Heating treatment (C= control; H = IR heating)"	-	-	-	-	-
Planting.date	yyyy-mm-dd	Planting time	-	PDATE	yyyy-mm-dd	-	-
date	yyyy-mm-dd	Date of simulation	-	DATE	yyyy-mm-dd	-	-
DAS	day	Days after sowing	DAP	-	-	Date difference function(DATE, PDATE)	-
Emergence.date	yyyy-mm-dd	Time to crop emergence (DC10)	-	PLDAE	yyyy-mm-dd	-	-
Ant.date	yyyy-mm-dd	Time to anthesis (DC65)	-	ADAT	yyyy-mm-dd	-	-
Mat.date	yyyy-mm-dd	Time to maturity (DC90)	-	MDAT	yyyy-mm-dd	-	-
Haun	leaf/mainstem	Leaf number as Haun stage	-	LNUM	code	-	-
FLN	leaf/mainsteam	Mean of final leaf number	-	LnoSM	-	-	-
GrainDM	t DM/ha	In season grain dry mass	-	GWAD	kg/ha	1000	-
Yield	t DM/ha	Grain dry mass at harvest	-	GWAM	kg/ha	1000	-
Biom	t DM/ha	Total above ground biomass	-	CWAD	kg/ha	1000	-
Biom.an	t DM/ha	Total above ground biomass at anthesis	-	CWAA	kg/ha	1000	-
Biom.ma	t DM/ha	Total above ground biomass at harvest	-	CWAM	kg/ha	1000	-
HI	%	Dry mass harvest index at maturity	100 * Yield / Biom.ma	HIAM	fraction	0.01	-
GNumber	grain/m�	Grain number	-	H#AM	grain/m�	1	-
GDM	mg DM/grain	In-season single grain dry mass	10^5 * Yield / GNumber	GWGD	mg	1	-
GDM.ma	mg DM/grain	Single grain dry mass at maturity	10^5 * GrainDM / GNumber	GWGM	mg	1	-
LAI	m�/m�	In-season leaf area index	-	LAID	m�/m�	1	-
CroN	kg N/ha	In-season total above ground nitrogen	-	CNAD	kg N/ha	1	-
CroN.an	kg N/ha	Total above ground nitrogen at anthesis	-	CNAA	kg N/ha	1	-
CroN.ma	kg N/ha	Total above ground nitrogen at maturity	-	CNAM	kg N/ha	1	-
GrainN	kg N/ha	In-season grain nitrogen	-	GNAD	kgN/ha	1	-
GrainN.ma	kg N/ha	Grain nitrogen at maturity	-	GNAM	kgN/ha	1	-
GNM	mg N/grain	In-season single grain nitrogen mass	100 * GrainN / GNumber	-	-	-	-
GNM.ma	mg N/grain	Single grain nitrogen mass at maturity	100 * GrainN.ma / GNumber	-	-	-	-
GPC	% of DM	Grain protein concentration	0.57 * GrainN.ma / Yield	GPRPCD	%	1	-
ET	mm	In-season cumulative evapotranspiration	-	ETCP	mm	1	-
ET.ma	mm	Cumulative evapotranspiration at maturity	-	ETCM	mm	1	-
Transp	mm	In-season cumulative transpiration	-	EPCM	mm	1	-
Transp.ma	mm	Cumulative transpiration at maturity	-	EPCP	mm	1	-
