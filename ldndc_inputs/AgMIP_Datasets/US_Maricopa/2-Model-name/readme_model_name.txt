"These files contain all data described in the data article: P. Martre, B. Kimball, J.W. White, S. Asseng, F. Ewert, M.J. Ottman, G.W. Wall, D. Cammarano, A.C. Ruane, P.K. Aggarwal, J. Anothai, B. Basso, C. Biernath, A.J. Challinor, G. De Sanctis, J. Doltr"
The Hot Serial Cereal Experiment for modeling wheat response to temperature: field experiments and AgMIP-Wheat multi-model simulations
Open Data Journal for Agricultural Research : ODjAR  and is included in this root directory. 

"This folder: Model name contains model name, name code, and url/contact e-mail"

Data is provided  as tab delimited text files
HSC_AgMIP_model_name_url.txt

readme_model_name.txt
