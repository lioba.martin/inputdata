"These files contain all data described in the data article: P. Martre, B. Kimball, J.W. White, S. Asseng, F. Ewert, M.J. Ottman, G.W. Wall, D. Cammarano, A.C. Ruane, P.K. Aggarwal, J. Anothai, B. Basso, C. Biernath, A.J. Challinor, G. De Sanctis, J. Doltra, E. Fereres, M. Garcia-Vila, S. Gayler, G. Hoogenboom, L.A. Hunt, R.C. Izaurralde, M. Jabloun, C.D. Jones, K.C. Kersebaum, A.-K. Koehler, C. M�ller, S. Naresh Kumar, D.B. Lobell, A. Maiorano, C. Nendel, G. O�Leary, J.E. Olesen, T. Palosuo, E. Priesack, E. Eyshi Rezaei, D. Ripoche, R.P. R�tter, M.A. Semenov, I. Shcherbak, C. St�ckle, P. Stratonovitch, T. Streck, I. Supit, F. Tao, P. Thorburn, K. Waha,�, E. Wang, J. Wolf, Z. Zhao, and Yan Zhu"	
The Hot Serial Cereal Experiment for modeling wheat response to temperature: field experiments and AgMIP-Wheat multi-model simulations	
Open Data Journal for Agricultural Research : ODjAR  and is included in this root directory. 	

This folder: Weather data contains climatic data used as input for the simaulton runs	

Data is provided  as tab delimited text files formatted for input or as output of the R routines developed in the AgMIP 	
"effort on Crop growth model intercomparison effort (http://www.agmip.org/). See especially the ""tools"" section for further reference"	

Description of the weather files	
TRNO	Weather file
1	AZ000306.wth
4	AZ110306.wth
10	AZ000306.wth
13	AZ000306.wth
16	AZ000306.wth
31	AZ000306.wth
34	AZ310306.wth
40	AZ000306.wth
43	AZ000306.wth
46	AZ440306.wth
52	AZ000306.wth
55	AZ000306.wth
70	AZ000306.wth
73	AZ610306.wth
79	AZ000306.wth

readme_weather_data.txt	
