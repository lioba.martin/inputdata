File name	dataset name	description	doi	Citation	
HSC-AgMIP_data.rar	AgMIP HSC	purpose was model comparision	https://odjar.org/article/view/15830	Matre et al 2018
15829-Dataset-16512-1-4-20171120.rar	AgMIP HSC	puropse was experiment description, identical to above	https://odjar.org/article/view/15829	Kimball et al. 2018
# Both of the above dataset directories contain the exact same files, but the papers describe different files. It can be assumed that the data was later merged into the current format with 5 subfolders. Outputs (Experimental, Model) are mentioned in Matre et al., Inputs (treatments layout, soil, management) are described by Kimball et al. This info is also included in the Matre dataset, in form of the xml file.   
ISGHE-AgMIP_data.rar	AgMIP ISGHE	